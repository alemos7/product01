package com.productrate.assets.exceptions;

public class ErrorCatalogue {
    public static final String REGISTRATION_EXISTS = "0001";
    public static final String REGISTRATION_EXISTS_DESC = "The name of the super hero already exists";

    public static final String BAD_REQUEST = "400";
    public static final String BAD_REQUEST_DESC = "Error in the input parameters";

    public static final String DOES_NOT_EXIST = "404";
    public static final String DOES_NOT_EXIST_DESC = "The indicated record does not exist";
}
