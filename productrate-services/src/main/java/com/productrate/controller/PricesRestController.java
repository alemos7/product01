package com.productrate.controller;

import javax.validation.Valid;

import com.productrate.assets.annotations.ResponseTime;
import com.productrate.assets.exceptions.ErrorCatalogue;
import com.productrate.assets.exceptions.GeneralException;
import com.productrate.dto.request.PricesRequestDTO;
import com.productrate.dto.request.PricesSearchRequestDTO;
import com.productrate.entities.Prices;
import com.productrate.repository.BrandsRepository;
import com.productrate.repository.PricesRepository;
import com.productrate.repository.ProductsRepository;
import com.productrate.services.PricesSirvece;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.productrate.dto.response.PricesResponseDTO;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

/**
 *
 * @author alemos
 */

@EnableCaching
@RestController
@Slf4j
@RequestMapping("/api/v1/prices")
public class PricesRestController {

    @Autowired
    PricesRepository pricesRepository;

    @Autowired
    private PricesSirvece pricesService;

    @Autowired
    ProductsRepository productsRepository;

    @Autowired
    BrandsRepository brandsRepository;

    @GetMapping()
    public List<Prices> list() {
        return pricesRepository.findAll();
    }

    @ResponseTime
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    @CacheEvict(value = "character", allEntries = true)
    public Prices get(@PathVariable Long id) {

        if(!pricesService.existsById(id))
            throw new GeneralException(
                    ErrorCatalogue.DOES_NOT_EXIST,
                    NOT_FOUND,
                    ErrorCatalogue.DOES_NOT_EXIST_DESC);

        return pricesService.finById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable String id, @RequestBody Prices input) {
        return null;
    }

    @PostMapping
    public ResponseEntity<PricesResponseDTO> post(@Valid @RequestBody PricesRequestDTO request) {
        log.debug("Entering addQualifier [request]: {}", request);
        PricesResponseDTO savedDto = pricesService.savePrices(request);
        log.debug("Leaving addPrices [response]: {}", savedDto);
        return ResponseEntity.noContent().header("Location", String.valueOf(savedDto.getPrice())).build();
    }

    @GetMapping("/rate/search/")
    @ResponseStatus(OK)
    @Cacheable(value = "character")
    public PricesResponseDTO search(
            @RequestParam(required = true, name = "brand") Long brand,
            @RequestParam(required = true, name = "product") Long product,
            @RequestParam(required = true, name = "date") String date
    ) {
        PricesSearchRequestDTO dto = new PricesSearchRequestDTO();
        dto.setBrand(brand);
        dto.setProduct(product);
        dto.setDate(date);
        PricesResponseDTO price = pricesService.findByCriterial(dto);
        return price;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {
        return null;
    }


}
