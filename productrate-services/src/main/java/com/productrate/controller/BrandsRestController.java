package com.productrate.controller;

import com.productrate.entities.Brands;
import com.productrate.repository.BrandsRepository;
import com.productrate.services.BrandsSirvece;
import com.productrate.services.ProductsSirvece;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author sotobotero
 */
@RestController
@RequestMapping("/brand")
public class BrandsRestController {

    @Autowired
    BrandsRepository brandsRepository;

    @Autowired
    private BrandsSirvece brandsSirvece;

    @GetMapping()
    public List<Brands> list() {
        return brandsRepository.findAll();
    }

    @GetMapping("/{id}")
    public Brands get(@PathVariable Long id) {
        return brandsSirvece.finById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable String id, @RequestBody Brands input) {
        return null;
    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody Brands input) {
        Brands save = brandsRepository.save(input);
        return ResponseEntity.ok(save);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {
        return null;
    }


}
