package com.productrate.controller;

import com.productrate.repository.ProductsRepository;
import com.productrate.entities.Products;
import com.productrate.services.ProductsSirvece;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

/**
 *
 * @author sotobotero
 */
@RestController
@RequestMapping("/product")
public class ProductsRestController {

    @Autowired
    ProductsRepository productsRepository;

    @Autowired
    private ProductsSirvece productsSirvece;

    @GetMapping()
    public List<Products> list() {
        return productsRepository.findAll();
    }

    @GetMapping("/{id}")
    public Products get(@PathVariable Long id) {
        return productsSirvece.finById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable String id, @RequestBody Products input) {
        return null;
    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody Products input) {
        Products save = productsRepository.save(input);
        return ResponseEntity.ok(save);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {
        return null;
    }


}
