package com.productrate.services.impl;

import com.productrate.assets.exceptions.ErrorCatalogue;
import com.productrate.assets.exceptions.GeneralException;
import com.productrate.dto.request.PricesRequestDTO;
import com.productrate.dto.request.PricesSearchRequestDTO;
import com.productrate.dto.response.PricesResponseDTO;
import com.productrate.entities.Brands;
import com.productrate.entities.Prices;
import com.productrate.entities.Products;
import com.productrate.mapper.PricesMapper;
import com.productrate.repository.PricesRepository;
import com.productrate.services.BrandsSirvece;
import com.productrate.services.PricesSirvece;
import com.productrate.services.ProductsSirvece;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@Slf4j
public class PricesSirveceImpl implements PricesSirvece {

    @Autowired
    private PricesRepository pricesRepository;

    @Autowired
    private PricesMapper mapper;

    @Autowired
    private ProductsSirvece productsSirvece;

    @Autowired
    private BrandsSirvece brandsSirvece;

    @Transactional
    @Override
    public PricesResponseDTO savePrices(PricesRequestDTO dto){
        PricesResponseDTO savedDto=null;
        log.debug("Entering savePrices [dto]: {}",dto);

        Prices entity = mapper.dtoToEntity(dto);

        Products product = productsSirvece.finById(dto.getProduct());
        Brands brand = brandsSirvece.finById(dto.getBrand());

        entity.setStartDate(convertStringToTimestamp(dto.getStartDate()));
        entity.setEndDate(convertStringToTimestamp(dto.getEndDate()));
        entity.setProduct(product);
        entity.setBrand(brand);

        Prices saved = pricesRepository.save(entity);
        savedDto = mapper.entityToDto(saved);

        log.debug("Leaving saveQualifier [savedDto]: {}",savedDto);
        return savedDto;
    }

    @Transactional
    @Override
    public PricesResponseDTO findByCriterial(PricesSearchRequestDTO dto){
        List<Prices> price = pricesRepository.findByCriterial(dto.getBrand(), dto.getProduct(), convertStringToTimestamp(dto.getDate()));
        if(price.size()<=0) {
            throw new GeneralException(
                    ErrorCatalogue.DOES_NOT_EXIST,
                    NOT_FOUND,
                    ErrorCatalogue.DOES_NOT_EXIST_DESC);
        }

        PricesResponseDTO priceDTO = mapper.entityToDto(price.get(0));
        return priceDTO;
    }

    public Timestamp convertStringToTimestamp(String strDate) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parsedDate = dateFormat.parse(strDate);
            Timestamp timeStampDate = new Timestamp(parsedDate.getTime());
            return timeStampDate;
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            return null;
        }
    }

    @Override
    public Prices finById(Long id) {
        return pricesRepository.findById(id).orElseThrow();
    }

    @Override
    public Boolean existsById(Long id) {
        return pricesRepository.existsById(id);
    }

}
