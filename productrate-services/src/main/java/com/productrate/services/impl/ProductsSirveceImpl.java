package com.productrate.services.impl;

import com.productrate.entities.Products;
import com.productrate.repository.ProductsRepository;
import com.productrate.services.ProductsSirvece;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductsSirveceImpl implements ProductsSirvece {

    @Autowired
    private ProductsRepository productsRepository;

    @Override
    public Products finById(Long id) {
        return productsRepository.findById(id).orElseThrow();
    }
}
