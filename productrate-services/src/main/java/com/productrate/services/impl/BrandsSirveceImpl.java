package com.productrate.services.impl;

import com.productrate.entities.Brands;
import com.productrate.entities.Products;
import com.productrate.repository.BrandsRepository;
import com.productrate.repository.ProductsRepository;
import com.productrate.services.BrandsSirvece;
import com.productrate.services.ProductsSirvece;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BrandsSirveceImpl implements BrandsSirvece {

    @Autowired
    private BrandsRepository brandsRepository;

    @Override
    public Brands finById(Long id) {
        return brandsRepository.findById(id).orElseThrow();
    }
}
