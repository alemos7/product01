package com.productrate.services;

import com.productrate.dto.request.PricesRequestDTO;
import com.productrate.dto.request.PricesSearchRequestDTO;
import com.productrate.dto.response.PricesResponseDTO;
import com.productrate.entities.Prices;

import java.sql.Timestamp;

public interface PricesSirvece {

    PricesResponseDTO savePrices(PricesRequestDTO dto);

    PricesResponseDTO findByCriterial(PricesSearchRequestDTO dto);

    Prices finById(Long id);

    Boolean existsById(Long id);
}
