package com.productrate.mapper;

import com.productrate.dto.request.PricesRequestDTO;
import com.productrate.dto.response.PricesResponseDTO;
import com.productrate.entities.Prices;

public interface PricesMapper {
    PricesResponseDTO entityToDto(Prices entity);

    Prices dtoToEntity(PricesRequestDTO dto);
}
