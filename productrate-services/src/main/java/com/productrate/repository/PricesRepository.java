package com.productrate.repository;

import com.productrate.entities.Brands;
import com.productrate.entities.Prices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

public interface PricesRepository extends JpaRepository<Prices, Long> {

    @Query("SELECT p FROM Prices p WHERE p.brand.id = ?1 AND p.product.id = ?2  AND ?3  >= p.startDate AND ?3 <= p.endDate ORDER BY p.priority DESC")
    List<Prices> findByCriterial(Long brand, Long produc, Timestamp date);

}