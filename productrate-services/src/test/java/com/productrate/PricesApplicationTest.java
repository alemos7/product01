package com.productrate;

import com.productrate.dto.request.PricesSearchRequestDTO;
import com.productrate.services.PricesSirvece;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PricesApplicationTest {

    @Autowired
    PricesSirvece pricesServices;

    @Test
    void testPricefindByCriterial1() {
        PricesSearchRequestDTO dto = new PricesSearchRequestDTO();
        dto.setDate("2020-06-14 10:00:00");
        dto.setBrand(Long.valueOf(1));
        dto.setProduct(Long.valueOf(35455));

        var prices =  pricesServices.findByCriterial(dto);
        assertEquals((Double)35.5, prices.getPrice());
    }

}
