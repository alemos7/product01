package resources;
import com.productrate.entities.Brands;
import com.productrate.entities.Prices;
import com.productrate.entities.Products;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class Data {

    private static Date startDate = new Date();
    private static Date endDate = new Date(System.currentTimeMillis() + 86400 * 1000 * 2);

    public static Optional<Prices> getPrice1() {
        Prices prices = new Prices(
                1L,
                new Brands(1L, (String) "Zara"),
                new Products(35455L, (String) "MEDIAS"),
                new Timestamp(startDate.getTime()),
                new Timestamp(endDate.getTime()),
                1,
                1,
                35.5D,
                (String) "EUR"
        );
        return Optional.of(prices);
    }
}
